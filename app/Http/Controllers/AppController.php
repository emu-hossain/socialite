<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Email;
use Session;
use Redirect;
use DataTables;

class AppController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index(Request $request){
        // dd("sdssd");
        // dd($request);
        if($request->ajax())
        {
            $data = Email::all();
            return DataTables::of($data)
                    // ->addColumn('purpose', function($data){
                    //     $button = '<button type="button" name="edit" id="'.$data->id.'" class="edit btn btn-primary btn-sm">Edit</button>';
                    //     $button .= '&nbsp;&nbsp;&nbsp;<button type="button" name="edit" id="'.$data->id.'" class="delete btn btn-danger btn-sm">Delete</button>';
                    //     return $button;
                    // })
                    // ->rawColumns(['purpose'])
                    ->make(true);
        }
        return view('appData');
        // $allemails = Email::where('purpose', $appname)->paginate(8);
        // // dd($allemails);
        // return view('appData')->with('allemails', $allemails);
    }
}
