<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Email;
use Session;
use Redirect;
use GeoIP as GeoIP;

class EmailController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function emailSubmit(Request $request)
    {
        $data =geoip()->getLocation();

        dd($data);
        $email = new Email();
        $email->purpose = $request->purpose;
        $email->name = $request->name;
        $email->email = $request->email;
        // dd($name);
        $email->save();
        return back()->with('message', 'success');
        // return back();
    }

    public function alldata(){
        $allemails = Email::paginate(8);
        // dd($allemails);
        return view('emailList')->with('allemails', $allemails);
    }
}
