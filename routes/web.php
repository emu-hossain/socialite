<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/admin',[
    'as' => 'admin.login',
    'uses' => 'HomeController@index'
]);
// Route::get('/home', 'HomeController@index')->name('home');
Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
Route::post('/email/add',[
    'as' => 'user.email.submit',
    'uses' => 'EmailController@emailSubmit'
]);

Route::get('/email/form/data',[
    'as' => 'user.email.submit.data',
    'uses' => 'EmailController@alldata'
]);

Route::get('/apps/data', 'AppController@index');

